# Yahtzee game

**Note**

Player can roll less than 5 dice at once, therefore the domain model 
says player rolls 1 to 5 dice (according to the rules, player can reroll 
certain dice).
